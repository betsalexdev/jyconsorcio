const db = firebase.firestore();

const taskForm = document.getElementById('task-form')
const taskContainer = document.getElementById('tasks-container')


const create_entity = (serial,user,password,name,phone,address,status,branch) =>
    db.collection('entities').doc().set({
    serial,
    user,
    password,
    name,
    phone,
    address,
    branch,
    status,
    })
const getEntity = () => db.collection('entities').get();
const onGetEntities = (callback) => db.collection('entities').onSnapshot(callback);
const deleteEntity = (id) =>  db.collection('entities').doc(id).delete();

window.addEventListener('DOMContentLoaded', async(e)=>{
    onGetEntities((querySnapshot)=>{
        taskContainer.innerHTML = '';
        querySnapshot.forEach(doc=>{
            const ent = doc.data();
            ent.id = doc.id;
            taskContainer.innerHTML += `<div class="card card-body mt-2 border-primary">
            <h5 class="font-weight-bold">Serial:</h5>
            <p>${ent.serial}</p>
            <h5 class="font-weight-bold">Número de Banca:</h5>
            <p>${ent.user}</p>
            <h5 class="font-weight-bold">Clave:</h5>
            <p>${ent.password}</p>
            <h5 class="font-weight-bold">Nombre:</h5>
            <p>${ent.name}</p>
            <h5 class="font-weight-bold">Teléfono:</h5>
            <p>${ent.phone}</p>
            <h5 class="font-weight-bold">Dirreción:</h5>
            <p>${ent.address}</p>
            <h5 class="font-weight-bold">Consorcio:</h5>
            <p>${ent.branch}</p>
            <h5 class="font-weight-bold">Estado:</h5>
            <p>${ent.status}</p>
            <hr>
            <div>
            <button class="btn btn-primary btn-activate" data-id"${ent.id}">Activar</button>
            
            </div>
            </div>`;
            const btnsActivate = document.querySelectorAll('.btn-activate')
            btnsActivate.forEach(btn=>{
                btn.addEventListener('click',async (e)=>{
                  alert("Esta terminal esta siendo activada")
                    
                })
            })
        })

    })
    

})


taskForm.addEventListener('submit', async(e)=> {
    e.preventDefault();//cancele que la pagina actualize

    const serial = taskForm['serial'].value;
    const user = taskForm['user'].value;
    const password = taskForm['password'].value;
    const name = taskForm['name'].value;
    const phone = taskForm['phone'].value;
    const address = taskForm['address'].value;
    const status = taskForm['status'].value;
    const branch = taskForm['branch'].value;
    await create_entity(serial,user,password,name,phone,address,status,branch)
    var pass=  prompt("Por favor introduccir llave");
    if(pass=="X@sLuis0814"||"joseyami0827"){
        alert("Gracias, su terminal ha sido registrada estara activada en el proximo cierre")
        taskForm.reset()
        serial.focus();
    }else{
        alert("Llave incorrecta por favor vuelva a intentarlo")
    }
})